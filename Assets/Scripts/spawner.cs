﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour {
    public GameObject enemy;
    public float spawnTime;
    public Transform[] spawnPoints;


    void Start()
    {
        InvokeRepeating("Spawn", 2.0f, 5.0f);
    }


    void Spawn()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        enemy.SetActive(true);
        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        enemy.SetActive(false);

    }
}
