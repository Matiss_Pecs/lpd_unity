﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gun : MonoBehaviour {

    public float range = 100f;
    public int bulletsPerMag = 30;
    public int bulletsLeft;
    public int currentBullets;
    public float fireRate = 0.1f;
    public Transform shootPoint;
    float fireTimer;
    public float damage = 20f;
    public AudioClip aksSoundFile;
    public AudioClip aksRoloadFile;
    AudioSource mySound;
    public GameObject hitParticles;
    public GameObject kamera;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
            if (Input.GetButton("Fire1"))
            {
            if (currentBullets > 0)
                Fire(); //ja spiezham peli tad šaujam ar fire metodi
                 
            else
                Reload();
            }
        if (Input.GetKeyDown("r"))
            Reload();
        if (fireTimer < fireRate)
            fireTimer += Time.deltaTime;
    }
    
    private void Fire()
    {

        if (fireTimer < fireRate || currentBullets <= 0)
            {
                return;
            };
            mySound = GetComponent<AudioSource>(); 
            mySound.PlayOneShot(aksSoundFile, 0.3f); //atskanojam shaushanas efektu
            RaycastHit hit;

        if (Physics.Raycast(shootPoint.position, shootPoint.forward, out hit, range))
            {
                Debug.Log(hit.transform.name + "Trapijām!");
                GameObject hitParticleEffect = Instantiate(hitParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            }
            currentBullets--;
        Debug.Log(currentBullets + " lodes");
        fireTimer = 0.0f; //resetojam fire timer
        
        if(hit.transform.GetComponent<Health>())
        {
            hit.transform.GetComponent<Health>().ApplyDamage(damage);
        }
    }
    private void Reload()
    {
        mySound.PlayOneShot(aksRoloadFile, 0.3f);
        if (bulletsLeft <= 0) return;
        int bulletsToLoad = bulletsPerMag - currentBullets;
        int bulletsToDeduct = (bulletsLeft >= bulletsToLoad) ? bulletsToLoad : bulletsLeft; //if then bulletsload else bulletsleftpie

        bulletsLeft -= bulletsToDeduct;
        currentBullets += bulletsToDeduct;
        fireRate = 3.0f;
        StartCoroutine(reloadtiming());
    }
    IEnumerator reloadtiming()
    {
        yield return new WaitForSeconds(3);
        fireRate = 0.1f;
}
}
