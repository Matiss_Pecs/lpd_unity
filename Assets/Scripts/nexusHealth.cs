﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class nexusHealth : MonoBehaviour {
    [SerializeField] private float health = 100f;
    public Scene death;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ApplyDamage(float damage)
    {
        Debug.Log("NEXUS got hit" + damage);
        health = health - damage;
        if (health <= 0f)
        {
            SceneManager.LoadScene("main-menu");
        }
    }
}