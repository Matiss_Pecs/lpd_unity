﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class interpolacija : MonoBehaviour {
    float timeCounter = 0;
    float speed;
    float garums;
    float platums;
	// Use this for initialization
	void Start () {
        speed = 0.2f;
        garums = 50;
        platums = 50;
	}
	
	// Update is called once per frame
	void Update () {
        //interpolacija :)
        speed = Mathf.Lerp(0.2f, 0.5f, 60f);
        timeCounter += Time.deltaTime * speed;
        float x = Mathf.Cos(timeCounter) * garums;
        float y = 19;
        float z = Mathf.Sin(timeCounter) * platums;
        transform.position = new Vector3(60+x, y, 106+z);
	}
}
