﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemy : MonoBehaviour {
    public Transform Player;
    public int MoveSpeed = 4;
    public int MaxDist = 10;
    public int MinDist = 5;
    public Animator MyAnimator;
    public int damage = 10;
    bool attacks = true;
    public GameObject player;
    public Transform NEXUS;
    public GameObject NEXUScool;
    public AudioClip nexusHit;
    public AudioClip enemyHit;
    AudioSource mySound;
    public NavMeshAgent navMeshTest;
    // Use this for initialization
    void Start () {
        navMeshTest = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        
        if (Vector3.Distance(transform.position, Player.position) < Vector3.Distance(transform.position, NEXUS.position))
        {
            //transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            if (Vector3.Distance(transform.position, Player.position) < 4)
            {
                navMeshTest.velocity = Vector3.zero;
                navMeshTest.Stop();
                MyAnimator.SetBool("Moving", false);
            }
            else
            {
                navMeshTest.Resume();
                //transform.position += transform.forward * MoveSpeed * Time.deltaTime; //kustibai uz speletaju
                navMeshTest.SetDestination(player.transform.position);
                Debug.Log(navMeshTest.SetDestination(player.transform.position));
                MyAnimator.SetBool("Moving", true);
                
            }
        }
        else
        {
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            if (Vector3.Distance(transform.position, NEXUS.position) < 4)
            {
                navMeshTest.velocity = Vector3.zero;
                navMeshTest.Stop();
                MyAnimator.SetBool("Moving", false);
            }
            else
            {
                navMeshTest.Resume();
                //transform.position += transform.forward * MoveSpeed * Time.deltaTime;
                navMeshTest.SetDestination(NEXUS.transform.position);
                Debug.Log(navMeshTest.SetDestination(NEXUS.transform.position));
                MyAnimator.SetBool("Moving", true);
            }
        }

        StartCoroutine(Attack());
    }
    IEnumerator Attack()
    {
        float distance = Vector3.Distance(Player.transform.position, this.transform.position);
        float distance2 = Vector3.Distance(NEXUS.transform.position, this.transform.position);
        if (distance < 4 && attacks == true)
        {
            navMeshTest.velocity = Vector3.zero;
            navMeshTest.Stop();
            transform.LookAt(Player);
            attacks = false;
            MyAnimator.SetTrigger("Attack3Trigger");
            mySound = GetComponent<AudioSource>();
            mySound.PlayOneShot(enemyHit, 0.3f); //atskanojam shaushanas efektu
            player.GetComponent<charhealth>().ApplyDamage(damage);
            yield return new WaitForSeconds(1.2f);
            attacks = true;
        }
        else if (distance2 < 4 && attacks == true)
        {
            navMeshTest.velocity = Vector3.zero;
            navMeshTest.Stop();
            transform.LookAt(NEXUS);
            attacks = false;
            MyAnimator.SetTrigger("Attack3Trigger");
            mySound = GetComponent<AudioSource>();
            mySound.PlayOneShot(nexusHit, 0.3f); //atskanojam shaushanas efektu
            NEXUScool.GetComponent<nexusHealth>().ApplyDamage(damage);
            yield return new WaitForSeconds(1.2f);
            attacks = true;
        }
    }
}
