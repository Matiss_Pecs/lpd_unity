﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField] private float health = 100f;
    int kills = 0;
    public Animator MyAnimator;
    public void ApplyDamage(float damage)
    {
        Debug.Log("we got hit" + damage);
        health = health - damage;
        if (health <= 0f)
        {
            MyAnimator.SetTrigger("Death1Trigger");
            Collider m_Collider;
            m_Collider = GetComponent<Collider>();
            m_Collider.enabled = !m_Collider.enabled;
            Destroy(gameObject, 1);
            kills++;
        }
    }
}
